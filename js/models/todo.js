var ToDo = (function() {

    // CONSTRUCTOR
    /**
     *
     * @param       {string}    name
     * @param       {boolean}   [completed]
     * @constructor
     */
    function ToDo(name, completed) {
        var me = this;

        me.name = name;
        me.completed = Boolean(completed);
    }


    //PUBLIC METHODS

    ToDo.prototype.toggleCompleted = function() {
        var me = this;
        me.completed = !me.completed;
    };

    ToDo.prototype.getFormattedName = function(){
        var me = this;
        return me.name.replace(/^\s+|\s+$/g, '');
    };

    return ToDo;
})();