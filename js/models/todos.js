var ToDos = (function () {

    // CONSTRUCTOR
    /**
     *
     * @param           {[]}   [defaultToDos]
     * @constructor
     */
    function ToDos(defaultToDos) {
        var me = this;

        // CONSTANTS
        me.FILTER_SHOW_ALL          = 0;
        me.FILTER_SHOW_ACTIVE       = 1;
        me.FILTER_SHOW_COMPLETED    = 2;

        me.collection   = angular.isArray(defaultToDos) ? defaultToDos : [];
        me.filterState  = me.FILTER_SHOW_ALL;

        me.result       = [];
        me.left         = 0;

        if(me.collection.length){
            me.updateResult();
        }
    }


    //PUBLIC METHODS

    /**
     *
     * @param  {ToDo}   todo
     */
    ToDos.prototype.add = function(todo) {
        var me = this;

        if(!(todo instanceof ToDo)){
            return;
        }

        me.collection.unshift(todo);
        me.updateResult();
    };

    /**
     *
     * @param   {Number}   newFilterState
     */
    ToDos.prototype.changeFilterState = function(newFilterState){
        var me = this;

        me.filterState = newFilterState;
        me.updateResult();
    };

    ToDos.prototype.clearCompleted = function(){
        var me = this,
            newCollection = [];

        angular.forEach(me.collection, function(item){
            if(!item.completed){

                newCollection.push(item);
            }
        });

        me.collection = newCollection;
        me.updateResult();
    };

    ToDos.prototype.updateResult = function(){
        var me = this,
            result = [],
            left = 0;

        angular.forEach(me.collection, function(item){
            if(!item.completed) left++;

            if(me.filterState == me.FILTER_SHOW_ALL
                    || (me.filterState == me.FILTER_SHOW_ACTIVE && !item.completed)
                    || (me.filterState == me.FILTER_SHOW_COMPLETED && item.completed)){

                result.push(item);
            }
        });

        me.result = result;
        me.left = left;
    };

    return ToDos;
})();