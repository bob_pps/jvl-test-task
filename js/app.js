(function () {
    'use strict';

    angular.module('jvlTodos', []);

})();

// CONTROLLERS

(function () {
    'use strict';

    angular
        .module('jvlTodos')
        .controller('ToDoCtrl', ToDoCtrl);

    var lastEditedName;

    ToDoCtrl.$inject = ['$scope'];

    function ToDoCtrl($scope){

        $scope.addNewToDo   = addNewToDo;
        $scope.onBeforeEdit = onBeforeEdit;
        $scope.onAfterEdit  = onAfterEdit;

        $scope.todos = new ToDos([
            new ToDo('Buy flowers'),
            new ToDo('Drink Beer', true)
        ]);

        $scope.newToDo = new ToDo('');
        $scope.leftText = '';

        $scope.$watch('todos.left', changeLeft);



        function addNewToDo(){
            $scope.newToDo.name = $scope.newToDo.getFormattedName();

            if(!$scope.newToDo.name) return;

            $scope.todos.add(angular.copy($scope.newToDo));
            $scope.newToDo = new ToDo('');
        }

        /**
         * Save a name of the item before editing
         * @param   {ToDo}  todo
         */
        function onBeforeEdit(todo){
            lastEditedName = todo.name;
        }

        /**
         * Restore a name of the item after editing if new name is empty
         * @param   {ToDo}  todo
         */
        function onAfterEdit(todo){
            if(!todo.getFormattedName()){
                todo.name = lastEditedName;
            }
        }

        /**
         *
         * @param   {Number}    left
         */
        function changeLeft(left){
             $scope.leftText = left == 1
                ? left + ' item left'
                : left + ' items left';
        }
    }

})();

// DIRECTIVES

(function () {
    'use strict';

    angular
        .module('jvlTodos')
        .directive('enterPress', enterPress);

    function enterPress() {
        /*
         It adds a handling of Enter key press (use attr "enter-press='fnHandler()'")
         */
        var directive = {
            restrict: 'A',
            link    : link
        };

        return directive;

        function link(scope, element, attrs) {
            element.bind('keydown keypress', function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.enterPress);
                    });

                    event.preventDefault();
                }
            });
        }
    }
})();